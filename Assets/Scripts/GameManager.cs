using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject winScreen;
    [SerializeField] GameObject failScreen;

    [SerializeField] Rocket rocket;

    private void OnEnable()
    {
        rocket.onMoonReached += ShowWinScreen;
        rocket.onRocketDestroyed += ShowFailScreen;
    }

    void ShowFailScreen()
    {
        failScreen.SetActive(true);
    }

    void ShowWinScreen()
    {
        winScreen.SetActive(true);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
