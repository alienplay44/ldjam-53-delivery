using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float cameraFollowSpeed;
    [SerializeField] float cameraFollowTollerance;

    private void FixedUpdate()
    {
        if (Mathf.Abs(transform.position.y - target.position.y) >= cameraFollowTollerance)
        {
            Vector3 targetPos = new Vector3(transform.position.x, target.position.y, transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetPos, cameraFollowSpeed * Time.unscaledDeltaTime);
        }
    }

}
