using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class FloatingButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] UnityEvent trggerEvents;
    [SerializeField] float floatingSpeed;

    bool isButtonActive = false;
    bool isFloating = false;

    Rocket rocket;

    Vector3 floatingDirection;

    void OnEnable()
    {
        rocket = FindObjectOfType<Rocket>();
        rocket.onGravityDisabled += StartFloating;
    }

    void FixedUpdate()
    {
        if (isButtonActive)
        {
            trggerEvents.Invoke();
        }

        if (isFloating)
        {
            transform.localPosition += floatingDirection * floatingSpeed * Time.fixedUnscaledDeltaTime;

            if ((transform.localPosition.x >= CursorManager.instance.MaxX && floatingDirection.x > 0f) || 
                (transform.localPosition.x <= CursorManager.instance.MinX && floatingDirection.x < 0f))
            {
                floatingDirection.x = -floatingDirection.x;
            }

            if ((transform.localPosition.y >= CursorManager.instance.MaxY && floatingDirection.y > 0f) ||
                (transform.localPosition.y <= CursorManager.instance.MinY && floatingDirection.y < 0f))
            {
                floatingDirection.y = -floatingDirection.y;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            isButtonActive = true;
        }
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            CursorManager.instance.MovingButton(this);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            isButtonActive = false;
        }
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            CursorManager.instance.EndMovingButton();
        }
    }

    public void StartFloating()
    {
        isFloating = true;
        floatingDirection = Random.insideUnitCircle.normalized;
    }

}
