using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] int rocketLayer;
    [SerializeField] GameObject explosionEffect;
    [SerializeField] Vector3 movementDirection;
    [SerializeField] float speed;
    [SerializeField] float minX, maxX;

    private void Update()
    {
        transform.Translate(movementDirection.normalized * speed * Time.deltaTime, Space.World);

        if (transform.position.x >= maxX + 5f ||
            transform.position.x <= minX - 5f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == rocketLayer)
        {
            Rocket rocket = collision.gameObject.GetComponent<Rocket>();
            rocket.HitByObstacle();

            Destroy(this.gameObject);

            if (explosionEffect != null)
            {
                Instantiate(explosionEffect, transform.position, transform.rotation);
            }
        }
    }

}
