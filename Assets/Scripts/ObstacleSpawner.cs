using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField] GameObject[] obstacles;
    [SerializeField] float spawnPosOffsetX;
    [SerializeField] Transform[] spawnPointHeights;

    [SerializeField] int spawnObstaclesPerCooldown;
    [SerializeField] float spawnObstacleCooldown;
    float spawnTimer;

    private void Update()
    {
        spawnTimer += Time.deltaTime;

        if (spawnTimer >= spawnObstacleCooldown)
        {
            spawnTimer = 0f;
            SpawnObstacle();
        }
    }

    void SpawnObstacle()
    {
        for (int i = 0; i < spawnObstaclesPerCooldown; i++)
        {
            Instantiate(obstacles[Random.Range(0, obstacles.Length)],
                spawnPointHeights[Random.Range(0, spawnPointHeights.Length)].position + (Random.Range(0, 100) > 50 ? Vector3.right : Vector3.left) * spawnPosOffsetX, 
                transform.rotation);
        }
    }
    
}
