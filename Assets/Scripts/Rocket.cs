using System;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    Rigidbody2D rb;

    [SerializeField] float thrustForce;
    [SerializeField] float turnForce;
    [SerializeField] float noGravityPosY;
    [SerializeField] int groundLayer, moonLayer;
    [SerializeField] float minX, maxX;

    [SerializeField] float maxVelocity;

    bool noGravity = false;
    bool isDestroyed = false;
    bool hasWon = false;

    public event Action onGravityDisabled;
    public event Action onRocketDestroyed;
    public event Action onMoonReached;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (!noGravity && transform.position.y >= noGravityPosY) 
        {
            noGravity = true;
            onGravityDisabled?.Invoke();
        }

        if (transform.position.x >= maxX && rb.velocity.x > 0f)
        {
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
        else if (transform.position.x <= minX && rb.velocity.x < 0f)
        {
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!isDestroyed && collision.gameObject.layer == groundLayer)
        {
            if (transform.right == (Vector3)collision.contacts[0].normal || 
                -transform.right == (Vector3)collision.contacts[0].normal)
            {
                isDestroyed = true;
                onRocketDestroyed?.Invoke();
            }
        }

        if (!isDestroyed && !hasWon && collision.gameObject.layer == moonLayer)
        {
            hasWon = true;
            onMoonReached?.Invoke();
        }
    }

    public void Turn(bool right)
    {
        rb.AddTorque(right ? -turnForce : turnForce);
    }

    public void Thrust()
    {
        if (rb.velocity.magnitude < maxVelocity)
        {
            rb.AddForce(transform.up * thrustForce);
        }
    }

    public void HitByObstacle()
    {
        if (!hasWon)
        {
            onRocketDestroyed?.Invoke();
        }
    }

}
