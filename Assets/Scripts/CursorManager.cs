using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public static CursorManager instance;

    FloatingButton currentlyMovingButton;

    Vector2 previousMousePosition;
    bool isMovingMouse = false;

    [SerializeField] RectTransform uiHolder;

    [SerializeField] float timeScaleChangeSpeed;
    [SerializeField] float slowTimeScale;
    float timeScale = 1f;

    public float MinX, MaxX, MinY, MaxY;

    private void Awake()
    {
        instance = this;

        CalculateBounds();
    }

    void Update()
    {
        if (currentlyMovingButton is not null)
        {
            Vector2 mousePositionLocalSpace;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(currentlyMovingButton.transform.parent as RectTransform,
                Input.mousePosition, null, out mousePositionLocalSpace);

            currentlyMovingButton.transform.localPosition = mousePositionLocalSpace;
        }

        if (isMovingMouse || Input.GetMouseButton(0))
        {
            timeScale += timeScaleChangeSpeed * Time.unscaledDeltaTime;
        } 
        else
        {
            timeScale -= timeScaleChangeSpeed * Time.unscaledDeltaTime;
        }

        timeScale = Mathf.Clamp(timeScale, 0f, 1f);
        Time.timeScale = Mathf.Lerp(slowTimeScale, 1f, timeScale);
    }

    private void FixedUpdate()
    {
        Vector2 currentMousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        if (currentMousePosition == previousMousePosition)
        {
            isMovingMouse = false;
        }
        else
        {
            isMovingMouse = true;
        }

        previousMousePosition = currentMousePosition;
    }

    public void MovingButton(FloatingButton currentlyMovingButton)
    {
        if (currentlyMovingButton is not null)
        {
            this.currentlyMovingButton = currentlyMovingButton;
        }
    }

    public void EndMovingButton()
    {
        if (currentlyMovingButton is not null)
        {
            currentlyMovingButton.StartFloating();
            currentlyMovingButton = null;
        }
    }

    public bool IsCurrentlyMovingButton()
    {
        return currentlyMovingButton != null;
    }

    void CalculateBounds()
    {
        Vector2 corner1 = new Vector2(0f, 0f);
        Vector2 vector2 = new Vector2(Screen.width, Screen.height);

        Vector2 corner1LocalSpace;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(uiHolder, corner1, null, out corner1LocalSpace);

        Vector2 corner2LocalSpace;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(uiHolder, vector2, null, out corner2LocalSpace);

        MinX = corner1LocalSpace.x;
        MinY = corner1LocalSpace.y;

        MaxX = corner2LocalSpace.x;
        MaxY = corner2LocalSpace.y;
    }

}
